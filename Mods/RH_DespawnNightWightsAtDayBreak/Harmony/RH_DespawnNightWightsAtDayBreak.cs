﻿using Harmony;
using UnityEngine;
using DMT;
using System.Reflection;
using System.Collections.Generic;

public class RH_DespawnNightWightsAtDayBreak
{
    public class RH_DespawnNightWightsAtDayBreak_Init : IHarmony
    {
        public void Start()
        {
            Debug.Log(" Loading Patch : " + GetType().ToString());
            var harmony = HarmonyInstance.Create(GetType().ToString());
            harmony.PatchAll(Assembly.GetExecutingAssembly());
        }
    }

    [HarmonyPatch(typeof(EntityAlive))]
    [HarmonyPatch("CheckDespawn")]
    class PatchEntityAliveCheckDespawn
    {
        //NOTE : Requires zombies to be contain zombieWightNightWalker in name

        static void Postfix(EntityAlive __instance)
        {
            if (__instance.EntityClass.entityClassName.Contains("zombieWightNightWalker"))
            {
                float num3 = GameManager.Instance.World.GetWorldTime() / 24000f;
                float num2 = (num3 - (float)((int)num3)) * 24f;

                if (num2 > SkyManager.GetDawnTime() && num2 < SkyManager.GetDuskTime() && __instance.GetAttackTarget() == null && !__instance.DespawnChecked)
                {
                    __instance.DespawnChecked = true;
                    var gs = GetGamestageFromBounds(__instance.position.x, __instance.position.y, __instance.position.z, "DespawnNightWalkers");
                    //Log.Out("PatchEntityAliveCheckDespawn GS : " + gs);

                    if (gs >= 100)
                        return;

                    var rand = new System.Random().Next(1, 100);
                    //Log.Out("PatchEntityAliveCheckDespawn rand : " + rand);

                    if (gs < rand)
                    {
                        //Log.Out("CheckDespawn, Killing RH entity : " + __instance.EntityClass.entityClassName);
                        __instance.IsDespawned = true;
                        __instance.MarkToUnload(); 
                    }
                }
            }
        }
    }

    public static int GetGamestageFromBounds(float x, float y, float z, string source, bool log = false)
    {
        //if (log)
        //    Log.Out("BiomeSpawning ---------");

        // Get Players gamestage
        var chunkPosition = new Vector3(x, y, z);
        var bounds = new Bounds(new Vector3(chunkPosition.x, chunkPosition.y, chunkPosition.z), new Vector3(200f, 200f, 200f));
        List<Entity> playersInBounds = GameManager.Instance.World.GetEntitiesInBounds(typeof(EntityPlayer), bounds, new List<Entity>());
        List<int> partyLevelList = new List<int>();
        foreach (var entityBound in playersInBounds)
        {
            var player = entityBound as EntityPlayer;
            partyLevelList.Add(player.gameStage);

            //if (log)
            //    Log.Out("BiomeSpawning Player in bound : " + player.entityId + " : GS = " + player.gameStage + " : source = " + source);
        }

        if (playersInBounds.Count == 0)
            return 1;

        return Mathf.Max(GameStageDefinition.CalcPartyLevel(partyLevelList)/ playersInBounds.Count, 1);
    }
}
