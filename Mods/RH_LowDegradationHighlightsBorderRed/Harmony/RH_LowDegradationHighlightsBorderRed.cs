﻿using System;
using Harmony;
using UnityEngine;
using System.Reflection;
using DMT;

public class RH_LowDegradationHighlightsBorderRed
{
    public class RH_LowDegradationHighlightsBorderRed_Init : IHarmony
    {
        public void Start()
        {
            Debug.Log(" Loading Patch : " + GetType().ToString());
            var harmony = HarmonyInstance.Create(GetType().ToString());
            harmony.PatchAll(Assembly.GetExecutingAssembly());
        }
    }

    [HarmonyPatch(typeof(XUiC_ItemStack))]
    [HarmonyPatch("Update")]
    [HarmonyPatch(new Type[] { typeof(float) })]
    class PatchXUiC_ItemStackUpdate
    {
        static void Postfix(XUiC_ItemStack __instance, ref float _dt)
        {
            if (__instance.itemClass != null && !__instance.bLocked && !__instance.isDragAndDrop && !__instance.isOver)
            {
                if (__instance.itemStack.itemValue.HasQuality || __instance.itemStack.itemValue.HasModSlots || __instance.itemStack.itemValue.IsMod)
                {
                    if (__instance.durability != null && __instance.lockType != XUiC_ItemStack.LockTypes.Crafting)
                    {
                        if (__instance.itemStack.itemValue.PercentUsesLeft < 0.2f)
                        {
                            __instance.background.Color = new Color(1f, 0f, 0f);
                        }
                    }
                }
            }
        }
    }
}
