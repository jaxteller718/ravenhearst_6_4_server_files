﻿using Harmony;
using System.Collections.Generic;
using UnityEngine;
using UniLinq;
using DMT;
using System.Reflection;

public class RH_AirdropSpawnZeds
{
    public class RH_AirdropSpawnZeds_Init : IHarmony
    {
        public void Start()
        {
            Debug.Log(" Loading Patch : " + GetType().ToString());
            var harmony = HarmonyInstance.Create(GetType().ToString());
            harmony.PatchAll(Assembly.GetExecutingAssembly());
        }
    }

    [HarmonyPatch(typeof(EntitySupplyCrate))]
    [HarmonyPatch("OnUpdateEntity")]
    class PatchEntitySupplyCrateOnUpdateEntity
    {
        static bool Prefix(EntitySupplyCrate __instance)
        {
            if (__instance.lootContainer != null && __instance.lootContainer.bTouched && __instance.lootContainer.IsEmpty() && !__instance.bDead)
            {
                __instance.bDead = true;
                __instance.Health = 0;
                return true;
            }

            if (__instance.EntityName == "sc_General" && __instance.onGround && __instance.classMaxFood < GetMaxZombies() && (__instance.lootContainer == null || (__instance.lootContainer != null && !__instance.lootContainer.bTouched)))
            {
                var spawnRadius = 30f;

                if (__instance.IsBloodMoon)
                {
                    var closestplayerStill = __instance.world.GetClosestPlayer(__instance, spawnRadius, false);
                    if (closestplayerStill == null)
                    {
                        __instance.IsBloodMoon = false;
                    }
                    else
                        return true;
                }

                var closestplayer = __instance.world.GetClosestPlayer(__instance, 5f, false);
                if (closestplayer == null)
                    return true;

                BiomeSpawnEntityGroupList biomeSpawnEntityGroupList = BiomeSpawningClass.list["airdrop"];
                if (biomeSpawnEntityGroupList == null)
                    return true;

                __instance.IsBloodMoon = true;

                // Get Players gamestage   
                int partyLevel = GetGamestageFromBounds(__instance.position.x, __instance.position.y, __instance.position.z, "Airdrop");


                // Get list of biomeSpawnEntityGroup
                int matchingGameStage = 0;
                List<BiomeSpawnEntityGroupData> list = new List<BiomeSpawnEntityGroupData>();
                EDaytime edaytime = (!__instance.world.IsDaytime()) ? EDaytime.Night : EDaytime.Day;

                foreach (var x in biomeSpawnEntityGroupList.list.AsEnumerable().Reverse())
                {
                    //Finding matching gamestage
                    if (matchingGameStage == 0 && x.gameStage <= partyLevel)
                        matchingGameStage = x.gameStage;

                    if (matchingGameStage > 0)
                    {
                        if (x.gameStage != matchingGameStage || x.entityGroupRefName.Contains("Animals") || x.entityGroupRefName.Contains("Bandit")) break;
                        if (x.daytime == EDaytime.Any || x.daytime == edaytime)
                            list.Add(x);
                    }
                }
                list.Reverse();

                System.Random rnd = new System.Random();
                var spawnGroup = list.First();
                var group = EntityGroups.list[spawnGroup.entityGroupRefName];
                var rndEntityId = new System.Random();

                for (int i = 0; i < (spawnGroup.maxCount); i++)
                {
                    var randomPosition = __instance.GetPosition();
                    randomPosition.x = randomPosition.x + rnd.Next(-(int)spawnRadius / 2, (int)spawnRadius / 2);
                    randomPosition.z = randomPosition.z + rnd.Next(-(int)spawnRadius / 2, (int)spawnRadius / 2);
                    randomPosition.y = 0;

                    var blockPos = World.worldToBlockPos(randomPosition);
                    var chunk = (Chunk)__instance.world.GetChunkFromWorldPos(blockPos);
                    blockPos.y = (chunk.GetHeight(World.toBlockXZ((int)randomPosition.x), World.toBlockXZ((int)randomPosition.z)) + 1);
                    randomPosition.y = (float)blockPos.y;

                    Entity entityEnemy = EntityFactory.CreateEntity(group[rndEntityId.Next(group.Count - 1)].entityClassId, randomPosition);
                    GameManager.Instance.World.SpawnEntityInWorld(entityEnemy);
                    entityEnemy.SetSpawnerSource(EnumSpawnerSource.Dynamic);

                    //Log.Out(string.Concat(new object[] { "AIRDROP spawning : ", spawnGroup.entityGroupRefName, " for partylevel :", partyLevel, " - entity : ", entityEnemy.EntityClass.entityClassName }));

                    // Use this random field to count how many zeds have spawned...
                    __instance.classMaxFood++;

                    if (__instance.classMaxFood >= GetMaxZombies())
                        return true;
                }
            }

            return true;
        }

        public static int GetGamestageFromBounds(float x, float y, float z, string source, bool log = false)
        {
            //if (log)
            //    Log.Out("BiomeSpawning ---------");

            // Get Players gamestage
            var chunkPosition = new Vector3(x, y, z);
            var bounds = new Bounds(new Vector3(chunkPosition.x, chunkPosition.y, chunkPosition.z), new Vector3(200f, 200f, 200f));
            List<Entity> playersInBounds = GameManager.Instance.World.GetEntitiesInBounds(typeof(EntityPlayer), bounds, new List<Entity>());
            List<int> partyLevelList = new List<int>();
            foreach (var entityBound in playersInBounds)
            {
                var player = entityBound as EntityPlayer;
                partyLevelList.Add(player.gameStage);

                //if (log)
                //    Log.Out("BiomeSpawning Player in bound : " + player.entityId + " : GS = " + player.gameStage + " : source = " + source);
            }

            return Mathf.Max(GameStageDefinition.CalcPartyLevel(partyLevelList), 1);
        }

        private static int GetMaxZombies()
        {
            return GamePrefs.GetInt(EnumGamePrefs.MaxSpawnedZombies) > 100 ? 100 : GamePrefs.GetInt(EnumGamePrefs.MaxSpawnedZombies);
        }
    }
}
