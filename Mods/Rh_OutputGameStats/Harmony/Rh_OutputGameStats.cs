﻿using System;
using Harmony;
using UnityEngine;
using System.Reflection;
using DMT;
using System.Collections.Generic;

public class Rh_OutputGameStats
{
    public class Rh_OutputGameStats_Init : IHarmony
    {
        public void Start()
        {
            Debug.Log(" Loading Patch : " + GetType().ToString());
            var harmony = HarmonyInstance.Create(GetType().ToString());
            harmony.PatchAll(Assembly.GetExecutingAssembly());
        }
    }

    [HarmonyPatch(typeof(GameModeSurvival))]
    [HarmonyPatch("Init")]
    class PatchGameModeSurvivalInit
    {
        static void Postfix(GameModeSurvival __instance)
        {
            SortedList<string, string> sortedList = new SortedList<string, string>();
            for (int num = 0; num != GameStats.Instance.propertyValues.Length; num++)
            {
                string text = ((EnumGameStats)num).ToStringCached<EnumGameStats>();
                sortedList.Add(text, text + " = " + GameStats.GetObject((EnumGameStats)num));
            }
            foreach (string key in sortedList.Keys)
            {
                Log.Out(sortedList[key]);
            }
        }
    }
}
