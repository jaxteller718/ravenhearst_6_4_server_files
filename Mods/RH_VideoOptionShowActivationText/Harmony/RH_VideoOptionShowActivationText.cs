﻿using System;
using Harmony;
using System.Reflection;
using System.Collections.Generic;
using System.Reflection.Emit;
using System.Linq;
using UnityEngine;
using DMT;

public class RH_VideoOptionShowActivationText
{
    public class RH_VideoOptionShowActivationTextInit : IHarmony
    {
        public void Start()
        {
            Debug.Log(" Loading Patch : " + GetType().ToString());
            var harmony = HarmonyInstance.Create(GetType().ToString());
            harmony.PatchAll(Assembly.GetExecutingAssembly());
        }
    }

    [HarmonyPatch(typeof(XUiC_OptionsVideo))]
    [HarmonyPatch("Init")]
    class PatchXUiC_OptionsVideoInit
    {
        static void Postfix(XUiC_OptionsVideo __instance)
        {
            __instance.comboShowActivationText = __instance.GetChildById("ShowActivationText").GetChildByType<XUiC_ComboBoxBool>();
            __instance.comboShowActivationText.OnValueChangedGeneric += __instance.AnyPresetValueChanged;
        }
    }

    [HarmonyPatch(typeof(XUiC_OptionsVideo))]
    [HarmonyPatch("applyChanges")]
    public class PatchXUiC_OptionsVideoapplyChanges
    {
        static bool Prefix(XUiC_OptionsVideo __instance)
        {
            GamePrefs.Set(EnumGamePrefs.OptionsShowActivationText, (bool)__instance.comboShowActivationText.Value);

            return true;
        }
    }

    [HarmonyPatch(typeof(XUiC_OptionsVideo))]
    [HarmonyPatch("updateGraphicOptions")]
    public class PatchXUiC_OptionsVideoupdateGraphicOptions
    {
        static bool Prefix(XUiC_OptionsVideo __instance)
        {
            __instance.comboShowActivationText.Value = GamePrefs.GetBool(EnumGamePrefs.OptionsShowActivationText);

            return true;
        }
    }

    [HarmonyPatch(typeof(XUiC_InteractionPrompt))]
    [HarmonyPatch("Update")]
    [HarmonyPatch(new Type[] { typeof(float) })]
    class PatchXUiC_InteractionPromptUpdate
    {
        static void Postfix(XUiC_InteractionPrompt __instance, float _dt)
        {
            if (!GamePrefs.GetBool(EnumGamePrefs.OptionsShowActivationText))
            {
                __instance.Text = "";
                __instance.xui.playerUI.windowManager.Close(XUiC_InteractionPrompt.ID);
            }
        }
    }
    
}
