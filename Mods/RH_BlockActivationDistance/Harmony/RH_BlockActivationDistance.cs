﻿using System;
using Harmony;
using UnityEngine;
using System.Reflection;
using DMT;
using System.Collections.Generic;

public class RH_BlockActivationDistance
{
    public class RH_BlockActivationDistance_Init : IHarmony
    {
        public void Start()
        {
            Debug.Log(" Loading Patch : " + GetType().ToString());
            var harmony = HarmonyInstance.Create(GetType().ToString());
            harmony.PatchAll(Assembly.GetExecutingAssembly());
        }
    }

    [HarmonyPatch(typeof(Block))]
    [HarmonyPatch("CanPlaceBlockAt")]
    [HarmonyPatch(new Type[] { typeof(WorldBase), typeof(int), typeof(Vector3i), typeof(BlockValue), typeof(bool) })]
    public class PatchBlockCanPlaceBlockAt
    {
        public static bool Prefix(Block __instance, ref bool __result, ref WorldBase _world, ref int _clrIdx, ref Vector3i _blockPos, ref BlockValue _blockValue, bool _bOmitCollideCheck = false)
        {
            if (__instance.Properties.Values.ContainsKey("SingleCheckDistance"))
            {
                var activationDistance = 0;
                if (int.TryParse(__instance.Properties.Values["SingleCheckDistance"], out activationDistance))
                {
                    for (int i = _blockPos.x - activationDistance; i <= _blockPos.x + activationDistance; i++)
                    {
                        for (int j = _blockPos.z - activationDistance; j <= _blockPos.z + activationDistance; j++)
                        {
                            for (int k = _blockPos.y - activationDistance; k <= _blockPos.y + activationDistance; k++)
                            {
                                BlockValue block = _world.GetBlock(_clrIdx, new Vector3i(i, k, j));
                                if (Block.list[block.type].GetBlockName().Equals(__instance.GetBlockName()))
                                {
                                    __result = false;
                                    return false;
                                }
                            }
                        }
                    }
                }
            }

            return true;
        }
    }
}
