﻿using System;
using Harmony;
using System.Reflection;
using UnityEngine;
using DMT;

public class RH_StopQuestUpdateForSharedKills
{
    public class Init : IHarmony
    {
        public void Start()
        {
            Debug.Log(" Loading Patch : " + GetType().ToString());
            var harmony = HarmonyInstance.Create(GetType().ToString());
            harmony.PatchAll(Assembly.GetExecutingAssembly());
        }
    }


    // public void SharedKillClient(bool _isZombie, int _entityTypeID, int _xp, EntityPlayerLocal _player = null)

    [HarmonyPatch(typeof(GameManager))]
    [HarmonyPatch("SharedKillClient")]
    [HarmonyPatch(new Type[] { typeof(bool), typeof(int), typeof(int), typeof(EntityPlayerLocal) })]
    class PatchNetPackageEntityAwardKillServerProcessPackage
    {
        static bool Prefix(GameManager __instance, bool _isZombie, int _entityTypeID, int _xp, EntityPlayerLocal _player = null)
        {
            if (_player == null)
            {
                _player = __instance.myEntityPlayerLocal;
            }
            if (_player == null)
            {
                return false; ;
            }
            string entityClassName = EntityClass.list[_entityTypeID].entityClassName;
            _xp = _player.Progression.AddLevelExp(_xp, "_xpFromParty", Progression.XPTypes.Kill, true);
            _player.bPlayerStatsChanged = true;
            GameManager.ShowTooltip(_player, string.Format(Localization.Get("ttPartySharedXPReceived", ""), _xp));

            //if (_isZombie)
            //{
            //    QuestEventManager.Current.ZombieKilled(entityClassName);
            //    return;
            //}
            //QuestEventManager.Current.AnimalKilled(entityClassName);

            return false;
        }
    }
}
